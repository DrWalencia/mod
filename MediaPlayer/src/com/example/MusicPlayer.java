package com.example;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Chronometer;
import android.widget.SeekBar;
import android.widget.Toast;

public class MusicPlayer extends Activity implements Runnable {
   /** Called when the activity is first created. */
	
	/** The finite state machine responsible of playing the song*/
	private MediaPlayer mp_file;
	
	/** The chronometer which will be responsible for keeping the count of seconds*/
	private Chronometer mp_chronometer;
	
	/** The SeekBar which will show the progress of the song and also can be used to
	 * change the value of the current progress*/
	private SeekBar seekBar;
	
	/** It'll store the time gap between the chronometer is stopped and the next
	 * click from the user to play*/
	private long lastPause = 0;
	
	/** The method onPlayClick() will behave differently depending on the value of
	 * this variable:
	 * 
	 * 		-False: Set chronometer base to 0 and start playing.
	 * 
	 * 		-True: Set chronometer base to previousBase + currentTime - lastPause and start playing.
	 */
	private boolean isPaused = false;
	
	
   @Override
   public void onCreate(Bundle savedInstanceState) {
       	
	   	   super.onCreate(savedInstanceState);
           
	   	   setContentView(R.layout.main);  
     
	   	   /* Create a new MediaPlayer by loading the context and the resource ID corresponding
	   	    * to the file we want to load. Very rough and inflexible way of loading files. A lot
	   	    * of work can be done on it. */
           mp_file = MediaPlayer.create(this, R.raw.piano);

           /* Link the seekbar and the chronometer to the corresponding elements in the GUI*/
           seekBar = (SeekBar) findViewById(R.id.seekBar1);
           mp_chronometer = (Chronometer) findViewById(R.id.chronometer1);
           
           /* And set the position and limit of the seekbar */
           seekBar.setProgress(0);
           seekBar.setMax(mp_file.getDuration());
           
           
           /* Create and launch the thread that will keep the progressBar updated */
           Thread currentThread  = new Thread(this);
       	   currentThread.start();
       		
       	   /* This listener will reset the chronometer to the "zero position" when
       	      the song is finished.*/
       	   mp_file.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
				
				public void onCompletion(MediaPlayer mp) {
					mp_chronometer.stop();
					mp_chronometer.setBase(SystemClock.elapsedRealtime());
					
				}

			});
       	   
       	   
       	   /* This listener will make effective changes made by dragging the seekbar. The changes
       	    * will be in terms of playing time and chronometer time */
       	   seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

       		   public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
    			
    			if(fromUser){
    				
    				mp_file.seekTo(progress); // Update MediaPlayer
    				
    				/* If the change in progress is forwards, we have to sum the progress to the 
    				 * elapsed time of the system.*/
    				if (progress > seekBar.getProgress()){
    					mp_chronometer.setBase(SystemClock.elapsedRealtime() + progress);
    				
    				/* But if the change in progress is backwards, we have to substract the progress
    				 * to the elapsed time of the system. */
    				}else {
    					mp_chronometer.setBase(SystemClock.elapsedRealtime() - progress);
        			}
    				    				
    				seekBar.setProgress(progress); // Update seekBar
    			}
    		}

			public void onStartTrackingTouch(SeekBar seekBar) {
								
			}

			public void onStopTrackingTouch(SeekBar seekBar) {
								
			}
			
    	});


   }
   
   /**
    * Defines the actions to do when the button pause is clicked
    * @param view
    */
   public void onPauseClick(View view)  
   {  
	   
	   /* If the player is running we've work to do... */
	   if (mp_file.isPlaying()){
		   
		   mp_file.pause();
		   lastPause = SystemClock.elapsedRealtime(); // keep track of the moment we paused the player...
		   mp_chronometer.stop();
		   isPaused = true; // by giving to isPaused a true value, the method onPlayClick will know what to do...
		   
	   }else{
		   Toast.makeText(getApplicationContext(), "The player is not currently playing", Toast.LENGTH_SHORT).show();
	   }
       
   }
   
   /**
    * Defines the actions to do when the button Stop is clicked
    * @param view
    */
   public void onStopClick(View view)  
   {  
	   /* If the player is running we've work to do... */
	   if (mp_file.isPlaying()){
		
		   try {
			   
			   // Reset MediaPlayer
			   mp_file.stop();
			   mp_file.prepare();
			   mp_file.seekTo(0);
			   
			   // Reset chronometer
			   mp_chronometer.stop();
			   mp_chronometer.setBase(SystemClock.elapsedRealtime());
			   
			   // There's no need to reset the seekBar, the listener will do it... :D
			   
		   } catch (Exception e) {
			   e.printStackTrace();
		   }
		   
	   }else{
		   Toast.makeText(getApplicationContext(), "The player is already stopped", Toast.LENGTH_SHORT).show();
	   }
   }
   
   /**
    * Defines the actions to do when the button Play is clicked
    * @param view
    */
   public void onPlayClick(View view)  
   {  
	   /* If the player is not running we've work to do... */
	   if (!mp_file.isPlaying()){
		   
		   if ( isPaused ){
			   
			   // Reload the chronometer from the last position (before the pause) and start it
			   mp_chronometer.setBase(mp_chronometer.getBase() + SystemClock.elapsedRealtime() - lastPause);
			   mp_chronometer.start();
			   
			   mp_file.start();
			   
			   isPaused = false;
			   
		   }else {
			   
			   // Reset the chronometer to "zero position"
			   mp_chronometer.setBase(SystemClock.elapsedRealtime());
			   mp_chronometer.start();
			   
			   mp_file.start();
		   }
		   
	   }else {
		   Toast.makeText(getApplicationContext(), "The player is already running", Toast.LENGTH_SHORT).show();
	   }
   }
   
   
   /**
    * Defines the operation performed by the thread: update the seekBar with the current progress
    * and sleeps for 100ms
    */
   public void run() {
	
	try {
		
		while(mp_file != null){
			seekBar.setProgress(mp_file.getCurrentPosition());	
		}
		
		Thread.sleep(100);
		
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
	
}





}