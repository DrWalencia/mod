package com.android;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * Contains the relationship between the elements that comprise the UI and defines
 * the actions related to the different buttons, seekbar and spinners,
 * 
 * @author Pablo Valencia González
 *
 */
public class Draw extends Activity {
	
	/** The View in which the user is going to draw on */
    private DrawView _drawView;
    /** The seekbar that defines the stroke width */
    private SeekBar _dotSizeSeekBar;

    /**
     * Action performed when the activity is created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
    	super.onCreate(savedInstanceState);
    	
    	// set the application fullscreen and without titlebar. Load default layout.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main);
        
        _dotSizeSeekBar = (SeekBar) this.findViewById(R.id.strokeWidthSeekBar);
        _dotSizeSeekBar.setMax(100);
        _dotSizeSeekBar.setProgress(5);
        
        // create a new DrawView and add it to the FrameLayout in which the user
        // is supposed to draw on.
        _drawView = new DrawView(this,_dotSizeSeekBar.getProgress());
        FrameLayout fl = (FrameLayout) this.findViewById(R.id.frameLayout1);
        fl.addView(_drawView);
 
        // the listener that will update the seekbar when the user drags it.
        _dotSizeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				
				_drawView.setThickness(_dotSizeSeekBar.getProgress());
			}

			public void onStartTrackingTouch(SeekBar seekBar) {
				
			}

			public void onStopTrackingTouch(SeekBar seekBar) {
				
			}
			
		});
        
        // create the spinner for the color, load its values and put a custom listener on it.
        Spinner colorSpinner = (Spinner) findViewById(R.id.colorSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.colors_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        colorSpinner.setAdapter(adapter);
        colorSpinner.setOnItemSelectedListener(new ColorSelectedListener(_drawView));
        
        // create the spinner for the tool, load its values and put a custom listener on it.
        Spinner toolSpinner = (Spinner) findViewById(R.id.toolSpinner);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.tools_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        toolSpinner.setAdapter(adapter2);
        toolSpinner.setOnItemSelectedListener(new ToolSelectedListener(_drawView,this));
 
    }
    
    /**
     * Returns the seekBar related to the current Draw object.
     */
    public SeekBar getSeekBar(){
  
    	return _dotSizeSeekBar;
    }
    
    /**
     * Action performed when the Clear button is clicked.
     */
    public void onClearClick(View view){  
    	
    	_drawView.clearScreen();
    }
    
    /**
     * Action performed when the Undo button is clicked.
     */
    public void onUndoClick(View view)  {  
    	
    	_drawView.undo();
    }
    
    /**
     * Action performed when the Redo button is clicked.
     */
    public void onRedoClick(View view)  {  
    	
    	_drawView.redo();
    }
}

/**
 * Custom listener that will take care of the ToolSpinner.
 * 
 * @author Pablo Valencia González
 */
class ToolSelectedListener implements OnItemSelectedListener {
	
	/** The View in which the tool is going to be changed */
	private DrawView _drawView;
	/** The seekBar that'll be enabled or disabled depending on the selected tool */
	private SeekBar _sb;

	/**
	 * Creates a newly allocated ToolSelected listener initializing its fields 
	 */
	public ToolSelectedListener(DrawView drawView, Draw draw) {
		
		_drawView = drawView;
		_sb = draw.getSeekBar();
	}

	/**
	 * Defines what to do when a given tool is selected.
	 */
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		
		// get the selected item..
		String tool = arg0.getItemAtPosition(arg2).toString();
		
		if (tool.compareTo("Point/Line") == 0){
			_drawView.setActionToPerform(1);
			_sb.setEnabled(true); // the seekBar is needed to change the thickness of the line/size of the point
		}else if (tool.compareTo("Circle") == 0) {
			_drawView.setActionToPerform(2);
			_sb.setEnabled(false); // there's no need for the seekBar when we're drawing circles
			
		}else {
			Toast.makeText(_drawView.getContext(), "Wrong tool selected", Toast.LENGTH_SHORT).show();
		}
	}

	public void onNothingSelected(AdapterView<?> arg0) {
		
	}

}

/**
 * Custom listener that will take care of the colorSpinner
 * 
 * @author Pablo Valencia González
 */
class ColorSelectedListener implements OnItemSelectedListener {
	
	/** The View in which the color is going to be changed */
	private DrawView _drawView;

	/** Creates a newly allocated ColorSelected listener initializing its fields */
	public ColorSelectedListener(DrawView drawView) {
		
		_drawView = drawView;
	}

	/** Defines what to do when a given color is selected */
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		
		String color = arg0.getItemAtPosition(arg2).toString();
		
		if (color.compareTo("White") == 0){
			_drawView.setColor(Color.WHITE);
		}else if (color.compareTo("Blue") == 0) {
			_drawView.setColor(Color.BLUE);
		}else if (color.compareTo("Cyan") == 0) {
			_drawView.setColor(Color.CYAN);
		}else if (color.compareTo("Dark Gray") == 0) {
			_drawView.setColor(Color.DKGRAY);
		}else if (color.compareTo("Gray") == 0) {
			_drawView.setColor(Color.GRAY);
		}else if (color.compareTo("Green") == 0) {
			_drawView.setColor(Color.GREEN);
		}else if (color.compareTo("Light Gray") == 0) {
			_drawView.setColor(Color.LTGRAY);
		}else if (color.compareTo("Magenta") == 0) {
			_drawView.setColor(Color.MAGENTA);
		}else if (color.compareTo("Red") == 0) {
			_drawView.setColor(Color.RED);
		}else if (color.compareTo("Yellow") == 0) {
			_drawView.setColor(Color.YELLOW);
		}else {
			Toast.makeText(_drawView.getContext(), "Wrong color selected", Toast.LENGTH_SHORT).show();
		}

		
	}

	public void onNothingSelected(AdapterView<?> arg0) {
		
		
	}

}







