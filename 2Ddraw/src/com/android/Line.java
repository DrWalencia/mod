package com.android;

/**
 * Defines what comprises a "Line" object and provides an interface to 
 * retrieve its properties.
 * 
 * @author Pablo Valencia González
 *
 */
public class Line {
	
	/** The point that begins the line */
	private Point _firstPoint;
	/** The point that ends the line */
	private Point _secondPoint;
	/** The thickness of the line */
	private int _thickness;
	/** The color of the line */
	private int _color;
	/** The type of the line: if it's just a segment or the diameter of a circle */
	private int _type;
	
	/**
	 * Creates a newly allocated Line object initializing its fields.
	 */
	Line ( Point point1, Point point2, int dotSize, int color, int type  ){
		_firstPoint = point1;
		_secondPoint = point2;
		_thickness = dotSize;
    	_color = color;
    	_type = type;
	}
	
	public Point getFirstPoint(){
		return _firstPoint;
	}
	
	public Point getSecondPoint(){
		return _secondPoint;
	}
	
	public int getDotSize(){
    	return _thickness;
    }
    
    public int getColor(){
    	return _color;
    }
    
    public int getType(){
    	return _type;
    }
	
}
