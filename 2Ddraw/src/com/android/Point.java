package com.android;

/**
 * A "Point" is composed by an horizontal coordinate and a vertical
 * coordinate. Provides an interface to store and retrieve the 
 * mentioned coordinates.
 * 
 * @author Pablo Valencia González
 *
 */
public class Point {
	
	private float _xCoordinate, _yCoordinate;
	
	
    Point( float xC, float yC){
    	_xCoordinate = xC;
    	_yCoordinate = yC;
    	
    }
    
    public float getX(){
    	return _xCoordinate;
    }
    
    public float getY(){
    	return _yCoordinate;
    }
    
    
}