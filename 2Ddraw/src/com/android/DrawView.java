package com.android;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Toast;

/**
 * Custom View that defines the different modes of drawing that will be allowed
 * and also external operations that can be performed on it.
 * 
 * @author Pablo Valencia González
 *
 */
public class DrawView extends View implements OnTouchListener {
    
	/** Operating mode 0: clear the View */
	private final int CLEAR = 0;
	/** Operating mode 1: draw lines, or if the two points that comprise the 
	 * line are the same point, draw a point */
	private final int POINTS_LINES = 1;
	/** Operating mode 2: draw circles. The two points of the line specify the
	 * diameter in this mode */
	private final int CIRCLES = 2;

	/** This point will hold the first point of a line until the finger is released
	 *  from the screen. After that, a new line is created. */
	private Point _pointAux;
	
	/** List of lines that are being drawn on the View */
    private List<Line> _lineList = new ArrayList<Line>();
    /** List of lines that are being removed from the View via the undo() method */
    private List<Line> _redoList = new ArrayList<Line>();

    /** Defines what to do when a line is retrieved from the view: Draw a circle or draw a point/line */
    private int _actionToPerform;
    /** Flag that will determine when the undo() operation can be performed */
    private boolean _enableRedo;
    /** The thickness of the line. Useless in case a circle is drawn */
    private int _thickness;
    /** The color of the line */
    private int _color;

    /**
     * Creates a newly allocated DrawView object initializing its fields and setting different options.
     */
    public DrawView(Context context, int thickness) {
    	
        super(context);
        setFocusable(true);
        setFocusableInTouchMode(true);

        this.setOnTouchListener(this);

        _thickness = thickness;
        _enableRedo = false;
        
    }

    /**
     * Sets the elements to be drawn on the View
     * @param action code for the operation
     */
    public void setActionToPerform(int action) {
		_actionToPerform = action;
		
	}

    /**
     * Sets the color for the elements that are going to be drawn
     * @param color code for the color
     */
	public void setColor(int color) {
    	_color = color;
		
	}

	/**
	 * Sets the thickness of the lines that are going to be drawn. 
	 * In case we're drawing circles, it won't be used.
	 * @param scrollX the value retrieved from the seekBar
	 */
	public void setThickness(int scrollX) {
    	_thickness = scrollX;
		
	}

	/**
	 * Performs the redo action if there's something to redo and we're coming from an
	 * undo action, imitating the same behavior as The Gimp. Otherwise, a Toast message
	 * will be shown.
	 */
	public void redo() {
		
		if ( ( _redoList.size() > 0 ) && _enableRedo ) {
			
			_lineList.add(_redoList.get(_redoList.size()-1));
			_redoList.remove(_redoList.size()-1);
			invalidate();
			
		}else{
			
			Toast.makeText(this.getContext(), "Nothing to redo", Toast.LENGTH_SHORT).show();
		}
		
	}

	/**
	 * Performs the undo action if there's something to be undone. Otherwise a toast message will
	 * be shown.
	 */
	public void undo() {
		
		if (_lineList.size() == 0){
			
			Toast.makeText(this.getContext(), "Nothing to undo", Toast.LENGTH_SHORT).show();
			
		}else {
			
			_redoList.add(_lineList.get(_lineList.size()-1));
			_lineList.remove(_lineList.size()-1);
			_enableRedo = true;
			invalidate();
			
		}
	}

	/**
	 * Sets the action to perform as CLEAR, and then after the View is cleared, the
	 * previous action to perform is recovered.
	 */
	public void clearScreen() {
    	
		int aux = _actionToPerform;
		_actionToPerform = CLEAR;
		invalidate();
		_actionToPerform = aux;
	}

	/**
	 * Draws according to the contents of the fields.
	 */
	@Override
    public void onDraw(Canvas canvas) {
		
		
		if (_actionToPerform == CLEAR ){
			
			canvas.drawColor(Color.BLACK);
			_lineList.removeAll(_lineList);
			
		}else {
			
			for( Line line: _lineList ){
				
				Paint p = new Paint();
				p.setColor(line.getColor());
				p.setAntiAlias(true);
				p.setStrokeWidth(line.getDotSize());
				
				switch(line.getType()){
				
					case POINTS_LINES:	// If both of the points that comprise a line are the same point, a point is drawn
										if ((( line.getFirstPoint().getX() - line.getSecondPoint().getX()) == 0) &&
											(( line.getFirstPoint().getY() - line.getSecondPoint().getY()) == 0)){
							
											canvas.drawCircle(line.getFirstPoint().getX(), line.getFirstPoint().getY(), (float) (line.getDotSize()/2), p);
											
										}else{
											// If not, a line between the points that comprise the line is drawn
											canvas.drawLine(line.getFirstPoint().getX(), line.getFirstPoint().getY(), line.getSecondPoint().getX(), line.getSecondPoint().getY(), p);
										}
						
										break;
					
					case CIRCLES:		// The line is used as the diameter of the circle wanted to be drawn
										Point middlePoint = getMiddlePoint(line);
										canvas.drawCircle(middlePoint.getX(), middlePoint.getY(), calculateRadius(line.getFirstPoint(),middlePoint), p);
						
										break;
				}
			}	
		}
    }

	/**
	 * Returns the middle point between the line that is being passed as a parameter.
	 * If a line is comprised by the points X and Y, the middle point will be: ( (X1+Y1)/2 , (X2+Y2)/2 )
	 */
    private Point getMiddlePoint(Line line) {
		
    	return new Point(( ( line.getFirstPoint().getX()+line.getSecondPoint().getX() ) /2 ), ( ( line.getFirstPoint().getY()+line.getSecondPoint().getY() ) /2 ));
	}

    /**
     * Returns the length between the two points passed as a parameter, by applying Pythagoras's theorem.
     */
	private float calculateRadius(Point firstPoint, Point middlePoint) {
		
    	float leg1 = middlePoint.getX()-firstPoint.getX();
    	float leg2 = middlePoint.getY()-firstPoint.getY();
    	
    	return (float) Math.sqrt( (leg1*leg1) + (leg2*leg2) );
	}

	/**
	 * Action to be performed whenever the DrawView is touched, to keep track of the points.
	 */
	public boolean onTouch(View view, MotionEvent event) {
    	
		/* In order to reproduce Gimp's undo/redo behavior, if redo can be done, but the user chooses to
		   draw again, the list of redoes must be erased, as done here. */
		if (_enableRedo) {
			_redoList.removeAll(_redoList);
		}
		
		/* Whenever a click is detected, track of the point is kept */
    	if (event.getAction() == MotionEvent.ACTION_DOWN){
    		_pointAux = new Point( event.getX() , event.getY());
    	}
    	
    	/* When a release is detected, the previous point is rescued in order to create a line between it and the
    	   release point */
    	if (event.getAction() == MotionEvent.ACTION_UP){
    		_lineList.add(new Line(_pointAux, new Point( event.getX() , event.getY()), _thickness , _color, _actionToPerform));
    		invalidate();
    	}
        
        return true;
    }
}