package com.android;

import java.util.ArrayList;
import java.util.Arrays;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class SwipingActivity extends ListActivity{
    
	
	private static final int SWIPE_MIN_DISTANCE = 80;
    private static final int SWIPE_MAX_OFF_PATH = 220;
    private static final int SWIPE_THRESHOLD_VELOCITY = 170;
    private GestureDetector _gestureDetector;
    private View.OnTouchListener _gestureListener;
    
    /** The adapter for our listView */
    private ArrayAdapter<String> _adapter;
    
    /** Our list of scrollable items */
    private ListView _lv;
    
    /** The element we want to eliminate */
    public String _element;
	
    /** The countries we're going to play with */
	private String[] _listOfCountries = new String[] {
	    "Albania", "Andorra","Armenia","Austria","Belarus", "Belgium", "Bulgaria", 
	    "Croatia", "Cyprus", "Czech Republic","Denmark","Estonia", "Finland",
	    "Macedonia", "France","Germany","Greece", "Hungary","Iceland", "Ireland", 
	    "Italy","Latvia", "Liechtenstein", "Lithuania", "Luxembourg","Moldova",
	    "Monaco","Netherlands","Norway", "Poland", "Portugal", "Romania",  
	    "San Marino","Slovakia", "Slovenia","Spain", "Sweden","Switzerland",
	     "Turkey","United Kingdom",
	    };
	
	/** The collection of countries that allows us to add/remove elements easily */
	private ArrayList<String> _countries = new ArrayList<String>(Arrays.asList(_listOfCountries));
	
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
    	super.onCreate(savedInstanceState);

    	// Create and set the adapter for our listView
    	_adapter = new ArrayAdapter<String> (this, R.layout.list_item, _countries);
    	setListAdapter(_adapter);
    	  
    	// Gesture detection
    	_gestureDetector = new GestureDetector(new MyGestureDetector());
    	_gestureListener = new View.OnTouchListener() {
    		public boolean onTouch(View v, MotionEvent event) {
    			return _gestureDetector.onTouchEvent(event);
    		}
    	};
	    
    	// Get the listView from the SwipingActivity class and put the gesture listener on it
	    _lv = getListView();
	    _lv.setOnTouchListener(_gestureListener);
	        
    }
    
    /** The class in charge of the gestures and its actions */
    class MyGestureDetector extends SimpleOnGestureListener {
    	
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            
        	try {
            	
                if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                    return false;
                
                
                // right to left swipe
                if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                	
                	_element = _adapter.getItem(_lv.pointToPosition((int) e1.getX(), (int) e1.getY()));
                	
                	// Create the dialog box
                    AlertDialog.Builder alertbox = new AlertDialog.Builder(SwipingActivity.this);
                    alertbox.setMessage("Are you sure you want to remove " + _element + "?");

                     // Set a positive/yes button and create a listener
                    alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                      
                    	public void onClick(DialogInterface arg0, int arg1) {
                        	
                    		// Remove the element and refresh the listView by setting a new adapter
                        	_countries.remove(_element);
                        	SwipingActivity.this.setListAdapter(new ArrayAdapter<String> (SwipingActivity.this, R.layout.list_item, _countries));
                        
                        }
                    });

                    // Set a negative/no button and create a listener
                    alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    	
                        public void onClick(DialogInterface arg0, int arg1) {
                        	
                        	// We do nothing...
                        	
                        }
                    });

                    alertbox.show();
       
                   // left to right swipe 
                }  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                	
                    Toast.makeText(SwipingActivity.this, "No Funcionality yet :)", Toast.LENGTH_SHORT).show();
                
                }
            
        	} catch (Exception e) {
            
            	e.printStackTrace();
            }
            
        	return false;
        }
        
    }

}